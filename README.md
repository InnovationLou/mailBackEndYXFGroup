## 运行流程

### 后台管理运行流程

系统只需要安装mysql、redis

- 启动nacos
- ![image-20201021100307328](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20201021100307328.png)



![image-20201021100330003](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20201021100330003.png)



![image-20201021100552389](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20201021100552389.png)

进入接口文档：http://localhost:8201/doc.html



登录

![image-20201021100951448](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20201021100951448.png)

![image-20201021101131381](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20201021101131381.png)

![image-20201021101658308](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20201021101658308.png)

## 系统搭建说明



```
mall
├── mall-common -- 工具类及通用代码模块
├── mall-mbg -- MyBatisGenerator生成的数据库操作代码模块
├── mall-auth -- 基于Spring Security Oauth2的统一的认证中心
├── mall-gateway -- 基于Spring Cloud Gateway的微服务API网关服务
├── mall-monitor -- 基于Spring Boot Admin的微服务监控中心
├── mall-admin -- 后台管理系统服务
├── mall-portal -- 移动端商城系统服务
├── mall-search -- 移动端商城搜索服务
└── config -- 配置中心存储的配置

```

|技术|版本|说明
| --- | --- | --- |
|Spring Boot|2.3.0|容器+MVC框架
Spring Security|5.1.4|认证和授权框架
MyBatis	3.4.6|ORM框架
MyBatisGenerator|1.3.3|数据层代码生成
PageHelper|5.1.8|MyBatis物理分页插件
Swagger-UI|2.9.2|文档生产工具
RabbitMq|3.7.14|消息队列
Redis|5.0|分布式缓存
MongoDb|4.2.5|NoSql数据库
Docker|18.09.0|应用容器引擎
Druid|1.1.10|数据库连接池
OSS	2.5.0|对象存储
JWT	0.9.0|JWT登录支持
Lombok|1.18.12|简化对象封装工具



## 所需安装环境



## [Mysql](http://www.macrozheng.com/#/deploy/mall_deploy_windows?id=mysql)

- 下载并安装mysql`5.7`版本，下载地址：https://dev.mysql.com/downloads/installer/
- 设置数据库帐号密码：root root
- 下载并安装客户端连接工具Navicat,下载地址：http://www.formysql.com/xiazai.html
- 创建数据库`mall`
- 导入document/sql下的`mall.sql`文件

## [Redis](http://www.macrozheng.com/#/deploy/mall_deploy_windows?id=redis)

- 下载Redis,下载地址：https://github.com/MicrosoftArchive/redis/releases

![img](http://www.macrozheng.com/images/mall_windows_deploy_04.png)

- 下载完后解压到指定目录；

![img](http://www.macrozheng.com/images/mall_windows_deploy_05.png)

- 在当前地址栏输入cmd后，执行redis的启动命令：`redis-server.exe redis.windows.conf`

![img](http://www.macrozheng.com/images/mall_windows_deploy_06.png)

## [Elasticsearch](http://www.macrozheng.com/#/deploy/mall_deploy_windows?id=elasticsearch)

- 下载Elasticsearch`7.6.2`的zip包，并解压到指定目录，下载地址：https://www.elastic.co/cn/downloads/past-releases/elasticsearch-7-6-2

![img](http://www.macrozheng.com/images/mall_windows_deploy_07.png)

- 安装中文分词插件，在`elasticsearch-7.6.2\bin`目录下执行以下命令：

```bash
elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.6.2/elasticsearch-analysis-ik-7.6.2.zipCopy to clipboardErrorCopied
```

![img](http://www.macrozheng.com/images/mall_windows_deploy_08.png)

- 运行bin目录下的`elasticsearch.bat`启动Elasticsearch服务。

![img](http://www.macrozheng.com/images/mall_windows_deploy_09.png)

## [Kibana](http://www.macrozheng.com/#/deploy/mall_deploy_windows?id=kibana)

- 下载Kibana，作为访问Elasticsearch的客户端，请下载`7.6.2`版本的zip包，并解压到指定目录，下载地址：https://www.elastic.co/cn/downloads/past-releases/kibana-7-6-2

![img](http://www.macrozheng.com/images/mall_windows_deploy_10.png)

- 运行bin目录下的`kibana.bat`，启动Kibana服务；

![img](http://www.macrozheng.com/images/mall_windows_deploy_11.png)

- 打开Kibana的用户界面，访问地址：[http://localhost:5601](http://localhost:5601/)

![img](http://www.macrozheng.com/images/mall_windows_deploy_12.png)

## [Logstash](http://www.macrozheng.com/#/deploy/mall_deploy_windows?id=logstash)

- 下载Logstash，用于收集日志，请下载`7.6.2`版本的zip包，并解压到指定目录，下载地址：https://www.elastic.co/cn/downloads/past-releases/logstash-7-6-2

![img](http://www.macrozheng.com/images/mall_windows_deploy_13.png)

- 将Logstash的配置文件`logstash.conf`拷贝到安装目录的`bin`目录下，配置文件地址：https://github.com/macrozheng/mall/blob/master/document/elk/logstash.conf

![img](http://www.macrozheng.com/images/mall_windows_deploy_14.png)

- Logstash需要安装json_lines插件。

```bash
logstash-plugin install logstash-codec-json_linesCopy to clipboardErrorCopied
```

- 运行bin目录下的`logstash.bat`，启动Logstash服务，启动命令如下：

```bash
logstash -f logstash.conf
```

## [Mongodb](http://www.macrozheng.com/#/deploy/mall_deploy_windows?id=mongodb)

- 下载MongoDB安装包，选择`Windows x64`版本安装，下载地址：https://www.mongodb.com/download-center/community

![img](http://www.macrozheng.com/images/mall_windows_deploy_15.png)

- 运行MongoDB安装包并选择自定义安装，设置好安装路径；

![img](http://www.macrozheng.com/images/mall_windows_deploy_16.png)

- 配置MongoDB，让MongoDB作为服务运行，并配置好数据目录和日志目录；

![img](http://www.macrozheng.com/images/mall_windows_deploy_17.png)

- 取消MongoDB Compass的安装选项（不取消安装极慢），需要可自行安装；

![img](http://www.macrozheng.com/images/mall_windows_deploy_18.png)

- 双击`mongo.exe`可以运行MongoDB自带客户端，操作MongoDB；

![img](http://www.macrozheng.com/images/mall_windows_deploy_19.png)

- 连接成功后会显示如下信息；

![img](http://www.macrozheng.com/images/mall_windows_deploy_20.png)

- 如果需要移除MongoDB服务，只需使用管理员权限运行`cmd`工具，并输入如下命令。

```bash
sc.exe delete MongoDBCopy to clipboardErrorCopied
```

- 下载客户端工具`Robo 3T`，下载地址：https://robomongo.org/download

![img](http://www.macrozheng.com/images/mall_windows_deploy_21.png)

- 下载完成后解压，双击`robo3t.exe`即可使用；

![img](http://www.macrozheng.com/images/mall_windows_deploy_22.png)

- 之后创建一个到MongoDB的连接；

![img](http://www.macrozheng.com/images/mall_windows_deploy_23.png)

- 创建连接成功以后，就可以操作MongoDB了。

![img](http://www.macrozheng.com/images/mall_windows_deploy_24.png)

## [RabbitMQ](http://www.macrozheng.com/#/deploy/mall_deploy_windows?id=rabbitmq)

- 安装Erlang，下载地址：http://erlang.org/download/otp_win64_21.3.exe

![img](http://www.macrozheng.com/images/mall_windows_deploy_25.png)

- 安装RabbitMQ，下载地址：https://dl.bintray.com/rabbitmq/all/rabbitmq-server/3.7.14/rabbitmq-server-3.7.14.exe

![img](http://www.macrozheng.com/images/mall_windows_deploy_26.png)

- 安装完成后，进入RabbitMQ安装目录下的sbin目录

![img](http://www.macrozheng.com/images/mall_windows_deploy_27.png)

- 在地址栏输入cmd并回车启动命令行，然后输入以下命令启动管理功能：

```
rabbitmq-plugins enable rabbitmq_management
```

![img](http://www.macrozheng.com/images/mall_windows_deploy_28.png)

- 访问地址查看是否安装成功：http://localhost:15672/

![img](http://www.macrozheng.com/images/mall_windows_deploy_29.png)

- 输入账号密码并登录：guest guest
- 创建帐号并设置其角色为管理员：mall mall

![img](http://www.macrozheng.com/images/mall_windows_deploy_30.png)

- 创建一个新的虚拟host为：/mall

![img](http://www.macrozheng.com/images/mall_windows_deploy_31.png)

- 点击mall用户进入用户配置页面

![img](http://www.macrozheng.com/images/mall_windows_deploy_32.png)

- 给mall用户配置该虚拟host的权限

![img](http://www.macrozheng.com/images/mall_windows_deploy_33.png)

- 至此，RabbitMQ的安装和配置完成。

## [OSS](http://www.macrozheng.com/#/deploy/mall_deploy_windows?id=oss)

### [开通OSS服务](http://www.macrozheng.com/#/deploy/mall_deploy_windows?id=开通oss服务)

- 登录阿里云官网；
- 将鼠标移至产品标签页，单击对象存储 OSS，打开OSS 产品详情页面；
- 在OSS产品详情页，单击立即开通。

### [创建存储空间](http://www.macrozheng.com/#/deploy/mall_deploy_windows?id=创建存储空间)

- 点击网页右上角控制台按钮进入控制台；

![img](http://www.macrozheng.com/images/mall_windows_deploy_34.png)

- 选择我的云产品中的对象存储OSS；

![img](http://www.macrozheng.com/images/mall_windows_deploy_35.png)

- 点击左侧存储空间的加号新建存储空间；

![img](http://www.macrozheng.com/images/mall_windows_deploy_36.png)

- 新建存储空间并设置读写权限为公共读。

![img](http://www.macrozheng.com/images/mall_windows_deploy_37.png)

### [跨域资源共享（CORS）的设置](http://www.macrozheng.com/#/deploy/mall_deploy_windows?id=跨域资源共享（cors）的设置)

- 选择一个存储空间，打开其基础设置；

![img](http://www.macrozheng.com/images/mall_windows_deploy_38.png)

- 点击跨越设置的设置按钮；

![img](http://www.macrozheng.com/images/mall_windows_deploy_39.png)

- 点击创建规则；

![img](http://www.macrozheng.com/images/mall_windows_deploy_40.png)

- 进行跨域规则设置；

![img](http://www.macrozheng.com/images/mall_windows_deploy_41.png)
package org.yxfgroup.mall.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * MyBatis相关配置
 * 
 */
@Configuration
@EnableTransactionManagement
@MapperScan({"org.yxfgroup.mall.mapper","org.yxfgroup.mall.dao"})
public class MyBatisConfig {
}

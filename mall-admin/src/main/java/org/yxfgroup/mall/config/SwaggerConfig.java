package org.yxfgroup.mall.config;

import org.yxfgroup.mall.common.config.BaseSwaggerConfig;
import org.yxfgroup.mall.common.domain.SwaggerProperties;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger API文档相关配置
 * 
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig extends BaseSwaggerConfig {

    @Override
    public SwaggerProperties swaggerProperties() {
        return SwaggerProperties.builder()
                .apiBasePackage("org.yxfgroup.mall.controller")
                .title("lala-mall后台系统")
                .description("lala-mall后台相关接口文档")
                .contactName("innovationLou")
                .version("1.0")
                .enableSecurity(true)
                .build();
    }
}

package org.yxfgroup.mall.service.impl;

import org.yxfgroup.mall.mapper.CmsPrefrenceAreaMapper;
import org.yxfgroup.mall.model.CmsPrefrenceArea;
import org.yxfgroup.mall.model.CmsPrefrenceAreaExample;
import org.yxfgroup.mall.service.CmsPrefrenceAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品优选Service实现类
 * 
 */
@Service
public class CmsPrefrenceAreaServiceImpl implements CmsPrefrenceAreaService {
    @Autowired
    private CmsPrefrenceAreaMapper prefrenceAreaMapper;

    @Override
    public List<CmsPrefrenceArea> listAll() {
        return prefrenceAreaMapper.selectByExample(new CmsPrefrenceAreaExample());
    }
}

package org.yxfgroup.mall.service;

import org.yxfgroup.mall.model.CmsPrefrenceArea;

import java.util.List;

/**
 * 优选专区Service
 * 
 */
public interface CmsPrefrenceAreaService {
    /**
     * 获取所有优选专区
     */
    List<CmsPrefrenceArea> listAll();
}

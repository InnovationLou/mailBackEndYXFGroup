package org.yxfgroup.mall.service.impl;

import org.yxfgroup.mall.mapper.OmsCompanyAddressMapper;
import org.yxfgroup.mall.model.OmsCompanyAddress;
import org.yxfgroup.mall.model.OmsCompanyAddressExample;
import org.yxfgroup.mall.service.OmsCompanyAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 收货地址管理Service实现类
 *
 */
@Service
public class OmsCompanyAddressServiceImpl implements OmsCompanyAddressService {
    @Autowired
    private OmsCompanyAddressMapper companyAddressMapper;
    @Override
    public List<OmsCompanyAddress> list() {
        return companyAddressMapper.selectByExample(new OmsCompanyAddressExample());
    }
}

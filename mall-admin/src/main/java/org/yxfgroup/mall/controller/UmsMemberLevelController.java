package org.yxfgroup.mall.controller;

import org.yxfgroup.mall.common.api.CommonResult;
import org.yxfgroup.mall.model.UmsMemberLevel;
import org.yxfgroup.mall.service.UmsMemberLevelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 会员等级管理Controller
 * 
 */
@Controller
@Api(tags = "会员等级积分管理", description = "会员等级积分管理")
@RequestMapping("/memberLevel")
public class UmsMemberLevelController {
    @Autowired
    private UmsMemberLevelService memberLevelService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiOperation("查询所有会员等级")
    @ResponseBody
    public CommonResult<List<UmsMemberLevel>> list(@RequestParam("defaultStatus") Integer defaultStatus) {
        List<UmsMemberLevel> memberLevelList = memberLevelService.list(defaultStatus);
        return CommonResult.success(memberLevelList);
    }

    @RequestMapping(value = "/integrationList",method = RequestMethod.GET)
    @ApiOperation("查询所有会员积分情况")
    @ResponseBody
    public CommonResult integrationList(){
        return CommonResult.success("");
    }

}

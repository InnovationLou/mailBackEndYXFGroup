package org.yxfgroup.mall.auth.config;

import org.yxfgroup.mall.common.config.BaseSwaggerConfig;
import org.yxfgroup.mall.common.domain.SwaggerProperties;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger API文档相关配置
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig extends BaseSwaggerConfig {

    @Override
    public SwaggerProperties swaggerProperties() {
        return SwaggerProperties.builder()
                .apiBasePackage("org.yxfgroup.mall.auth.controller")
                .title("lala mall认证中心")
                .description("lala mall认证中心相关接口文档")
                .contactName("innovationLou")
                .version("1.0")
                .enableSecurity(true)
                .build();
    }
}

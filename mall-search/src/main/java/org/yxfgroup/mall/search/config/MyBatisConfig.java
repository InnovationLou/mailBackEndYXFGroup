package org.yxfgroup.mall.search.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * MyBatis相关配置
 *
 */
@Configuration
@MapperScan({"org.yxfgroup.mall.mapper","org.yxfgroup.mall.search.dao"})
public class MyBatisConfig {
}
